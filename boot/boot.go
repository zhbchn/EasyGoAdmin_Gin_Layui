/**
 *
 * @author 摆渡人
 * @since 2021/8/20
 * @File : boot
 */
package boot

import (
	"fmt"
)

// 用于应用初始化
func init() {
	fmt.Println("应用初始化")
}
